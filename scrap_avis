#!/usr/bin/env python3

import dataclasses
import argparse
import os
import sys
from getpass import getpass


import mechanize


def validSemestre(semestre):
    if (
        not isinstance(semestre, str)
        or len(semestre) != 5
        or semestre[0] not in ("A", "P")
        or not semestre[1:].isdigit()
    ):
        raise TypeError
    return semestre


@dataclasses.dataclass
class PDFFile:
    filename: str
    content: bytes


def _formsetifnotexist(form, name, val):
    try:
        form.find_control(name).readonly = False
        form[name] = val
    except mechanize.ControlNotFoundError:
        form.new_control("hidden", name, {"value": val})


class LoginPasswdError(Exception):
    pass


class UVInconnue(Exception):
    pass


class PDFRetourEtuNotAvailable(Exception):
    pass


class SemestreInconnu(Exception):
    pass


class RecuperateurDAvis:
    def __init__(self, login, passwd):
        self._br = mechanize.Browser()
        self._br.open(
            "https://webapplis.utc.fr/smeappli/evaluation_uvs/resultats/index.xhtml"
        )
        form = self._br.forms()[0]
        form["username"] = login
        form["password"] = passwd
        req = form.click()
        try:
            rep = self._br.open(req)
        except:
            raise LoginPasswdError
        if rep is None or "mainForm" not in rep.read().decode():
            raise LoginPasswdError

    def set_semestre(self, semestre):
        if (
            len(semestre) != 5
            or semestre[0] not in ("A", "P")
            or not semestre[1:].isdigit()
        ):
            raise ValueError(f"Semestre {semestre!r} invalide")
        year = int(semestre[1:])
        if not (2000 <= year <= 2100):
            raise ValueError(f"Semestre {semestre!r} invalide")
        if semestre[0] == "P":
            sem = f"{year-1}03"
        else:
            sem = f"{year}01"
        self._br.open(
            "https://webapplis.utc.fr/smeappli/evaluation_uvs/resultats/index.xhtml"
        )
        form = self._br.forms()[0]
        try:
            form["mainForm:selPeriode"] = [sem]
        except mechanize.ItemNotFoundError:
            raise SemestreInconnu(semestre)
        _formsetifnotexist(form, "javax.faces.source", "mainForm:selPeriode")
        _formsetifnotexist(form, "javax.faces.behavior.event", "valueChange")
        form.fixup()
        req = form.click()
        self._br.open(req)
        form = self._br.forms()[0]
        self.UVs = {
            x.attrs["label"]: x.attrs["value"]
            for x in form.find_control("mainForm:selUv").get_items()
            if x.attrs["value"]
        }

    def get_UV_eval(self, uv, log=None):
        if uv not in self.UVs:
            raise UVInconnue(uv)

        if log is not None:
            log(f"Selection de l'UV {uv}")
        form = self._br.forms()[0]
        _formsetifnotexist(form, "javax.faces.source", "mainForm:selUv")
        _formsetifnotexist(form, "javax.faces.behavior.event", "valueChange")
        _formsetifnotexist(form, "mainForm:selUv", [self.UVs[uv]])
        form.fixup()
        req = form.click()
        self._br.open(req)

        if log is not None:
            log(f"Récupération de la page de l'UV {uv}")
        form = self._br.forms()[0]
        _formsetifnotexist(form, "javax.faces.source", "mainForm:cliFormuleResultat")
        _formsetifnotexist(form, "javax.faces.behavior.event", "click")
        form.fixup()
        req = form.click()
        fret = self._br.open(req)

        if log is not None:
            log(f"Récupération du pdf pour l'UV {uv}")
        form = self._br.forms()[0]
        _formsetifnotexist(form, "mainForm:txtRapport", "")
        _formsetifnotexist(form, "mainForm:cliPDFRetourEtu", "mainForm:cliPDFRetourEtu")
        form.fixup()
        req = form.click()
        try:
            fret = self._br.open(req)
        except:
            self._br.back()
            raise PDFRetourEtuNotAvailable(uv)

        fn = fret.info().get_filename()
        if fn is None or not fn.endswith(".pdf"):
            self._br.back()
            raise PDFRetourEtuNotAvailable(uv)
        ret = PDFFile(filename=fn, content=fret.read())
        self._br.back()
        return ret


def parseargs():
    parser = argparse.ArgumentParser(
        description="UTC PDFRetourEtu scrapper",
    )
    parser.add_argument(
        "--quiet",
        "-q",
        action="store_true",
        help="Quiet mode. Suppress message status on stderr.",
    )
    parser.add_argument(
        "--output",
        "-o",
        default=None,
        type=str,
        help="Si une seule UV: fichier de sortie, facultatif. "
        "Si plusieurs UVs: répértoire de sortie, obligatoire.",
    )
    parser.add_argument(
        "semestre", type=validSemestre, help="Semestre, de la forme A2021."
    )
    parser.add_argument(
        "UV", type=str, nargs="+", help='Nom de l\'UV. "all" désigne toutes les UVs.'
    )
    args = parser.parse_args()
    return args


def error(x):
    print("Erreur: " + x, file=sys.stderr)
    sys.exit(1)


def warning(x):
    print("Warning: " + x, file=sys.stderr)


def main():
    args = parseargs()

    if "all" in args.UV:
        UVs = None
    else:
        UVs = args.UV
    if UVs is None or len(UVs) > 1:
        if not args.output:
            error(
                "Pour de multiples UVs, un répertoire de sortie doit être indiqué avec --output."
            )

    if args.quiet:

        def log(x):
            return

    else:

        def log(x):
            print("→ " + x, file=sys.stderr)

    login = input("Login UTC: ")
    passwd = getpass("Password UTC: ", stream=sys.stderr)

    log("Connexion")
    try:
        ra = RecuperateurDAvis(login, passwd)
    except LoginPasswdError:
        error(f"Connexion impossible. Pas de réseau ou login/passwd incorrect")

    log(f"Selection du semestre {args.semestre}.")
    try:
        ra.set_semestre(args.semestre)
    except SemestreInconnu:
        error(f"Semestre {args.semestre!r} inconnu.")

    if UVs is None:
        UVs = ra.UVs.keys()

    multiple = (len(UVs) > 1)

    UVinconnues = [uv for uv in UVs if uv not in ra.UVs]
    if UVinconnues:
        warning(f"UVs inconnues: {UVinconnues!r}.")

    UVs = [uv for uv in UVs if uv not in UVinconnues]

    if not UVs:
        return

    if multiple:
        outputdir = args.output
        while outputdir.endswith(os.path.sep):
            outputdir = outputdir[: -len(os.path.sep)]
        if not outputdir:
            outputdir = os.path.sep
        if not os.path.exists(outputdir):
            os.mkdir(outputdir)
        else:
            if not os.path.isdir(outputdir):
                error(f"{outputdir!r} doit ne pas exister ou être un répertoire.")
    else:
        outputfile = args.output

    for uv in UVs:
        try:
            pdf = ra.get_UV_eval(uv, log=log)
        except PDFRetourEtuNotAvailable:
            log(f"{uv}: PDFRetourEtu non disponible.")
            continue
        if multiple:
            fn = os.path.join(outputdir, pdf.filename)
        else:
            if outputfile:
                fn = outputfile
            else:
                fn = pdf.filename
        log(f"Écriture de {fn!r}")
        with open(fn, "wb") as f:
            f.write(pdf.content)


if __name__ == "__main__":
    main()
